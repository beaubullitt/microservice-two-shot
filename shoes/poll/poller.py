import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "shoes_project.settings")
django.setup()

# Import models from hats_rest, here.
# from shoes_rest.models import Something
from shoes_rest.models import BinVO


def get_bins():
    response = requests.get("http://wardrobe-api:8000/api/bins/")
    print("✅Response is valid", response)

    content = json.loads(response.content)
    print("🪩 json content looks like:", content)

    for dict in content["bins"]:
        print("🦖dict is currently:", dict)
        BinVO.objects.update_or_create(
            import_href = dict["import_href"],
            bin_number = dict["bin_number"],
            bin_size = dict["bin_size"],
            defaults={"closet_name": dict["closet_name"]},
        )


def poll():
    while True:
        try:
            # Write your polling logic, here
            get_bins()
            print("🪩 get bins fxn has executed")
        except Exception as e:
            print("❌", e, file=sys.stderr)
        time.sleep(60)


if __name__ == "__main__":
    poll()
