from .models import Shoes, BinVO
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
import json

class BinVOEncoder(ModelEncoder):
    model = BinVO
    properties = [
        "import_href",
        "closet_name",
        "bin_number",
        "bin_size",
    ]

class ShoesEncoder(ModelEncoder):
    model = Shoes
    properties = [
        "id",
        "name",
        "binVO",
        "manufacturer",
        "color",
        "picture_url",
    ]
    encoders = {
        "binVO": BinVOEncoder(),
    }


@require_http_methods(["GET", "POST"])
def api_list_shoes(request):

    if request.method == "GET":
        shoes = Shoes.objects.all()
        return JsonResponse(
                {"shoes": shoes},
                encoder=ShoesEncoder,
            )

    else:
        content = json.loads(request.body)
        print("🚀", BinVO.objects.all())
        print("🩵", content)
        try:
            binVO_href = content["binVO"]
            binVO = BinVO.objects.get(import_href=binVO_href)
            content["binVO"] = binVO
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid BinVO id"},
                status=400,
            )
        shoe = Shoes.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoesEncoder,
            safe=False,
        )

@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_shoe(request, id):
    if request.method == "GET":
        shoe = Shoes.objects.get(id=id)
        # weather = get_weather_data(
        #     conference.location.city,
        #     conference.location.state.abbreviation,
        # )
        return JsonResponse(
            shoe,
            encoder=ShoesEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Shoes.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})

    else:
        content = json.loads(request.body)

        try:

            if "binVO" in content:
                binVO = BinVO.objects.get(import_href=content["href"])
                content["binVO"] = binVO
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )

        Shoes.objects.filter(id=id).update(**content)
        shoe = Shoes.objects.get(id=id)
        return JsonResponse(
            shoe,
            encoder=ShoesEncoder,
            safe=False,
        )
