import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "hats_project.settings")
django.setup()

from hats_rest.models import LocationVO

def get_locations():
    response = requests.get("http://wardrobe-api:8000/api/locations/")
    content = json.loads(response.content)
    # print(content)
    for i in content["locations"]:
        print(i)
        LocationVO.objects.update_or_create(
            href = i["href"],
            closet_name=i["closet_name"],
            section_number=i["section_number"],
            shelf_number=i["shelf_number"],
            defaults = {},
        )

def poll():
    while True:
        # print('Hats')
        try:
            print("TRY LINE POLLER LINE 31")
            get_locations()
        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(30)


if __name__ == "__main__":
    poll()
