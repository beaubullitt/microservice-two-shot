from django.db import models
from django.urls import reverse

# Create your models here.

# python manage.py makemigrations
# It is impossible to add a non-nullable field 'href' to locationvo without specifying a default. This is because the database needs something to populate existing rows.
# Please select a fix:
#  1) Provide a one-off default now (will be set on all existing rows with a null value for this column)
#  2) Quit and manually define a default value in models.py.

class LocationVO(models.Model):
    href = models.CharField(max_length=180,null=True, unique=True)
    closet_name = models.CharField(max_length =120)
    section_number = models.SmallIntegerField()
    shelf_number = models.SmallIntegerField()

    def get_api_url(self):
        return reverse("api_location", kwargs={"pk": self.pk})

    def __str__(self):
        return f"{self.closet_name} - {self.section_number}/{self.shelf_number}"

    class Meta:
        ordering = ("closet_name", "section_number", "shelf_number")





class Hat(models.Model):
    fabric = models.CharField(max_length=150)
    style_name = models.CharField(max_length=150)
    color = models.CharField(max_length=150)
    picture_url = models.URLField()
    location = models.ForeignKey(
        LocationVO,
        related_name="location",
        on_delete=models.CASCADE
    )
    def __str__(self):
        return f"{self.style_name}"
    def get_api_url(self):
        return reverse("api_show_hat", kwargs={"pk": self.pk})
