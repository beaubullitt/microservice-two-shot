from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from common.json import ModelEncoder
from .models import LocationVO, Hat

class LocationVOEncoder(ModelEncoder):
    model = LocationVO
    properties= [
        "href",
        "closet_name",
        "section_number",
        "shelf_number",

    ]


class HatListEncoder(ModelEncoder):
    model = Hat
    properties =[
        "fabric",
        "style_name",
        "color",
        "picture_url",
        "location"

    ]
    encoders ={
        "location":LocationVOEncoder()
    }


class HatDetailEncoder(ModelEncoder):
    model = Hat
    properties =[
        "id",
        "style_name",
        "color",
        "picture_url",
        "location"

    ]
    encoders ={
        "location":LocationVOEncoder()
    }
# location_id = content["location"]


@require_http_methods(["GET", "POST"])
def api_list_hats(request):

    if request.method =="GET":
        hats= Hat.objects.all()
        return JsonResponse(
            {"hats":hats},
            encoder=HatListEncoder)
    else:
        content = json.loads(request.body)
        print(content)

        try:
            location = LocationVO.objects.get(id =content['location'])
            print(location)
            content["location"]= location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message":"invalid loction ID"},
                status = 400
            )


        hat = Hat.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False
        )


@require_http_methods(["GET","DELETE"])
def api_show_hat(request,id):
    if request.method == "GET":
        try:
            hat = Hat.objects.get(id=id)
            return JsonResponse(
                {"hat":hat},
                encoder=HatDetailEncoder,
                safe=False
            )
        except Hat.DoesNotExist:
            response = JsonResponse({"message":"This does not exist, No detial view available"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            hat = Hat.objects.get(id=id)
            hat.delete()
            return JsonResponse({"deleted":"GONE"})
        except Hat.DoesNotExist:
            return JsonResponse({"Message":"Object does not exist therefore cannot be deleted"})
